# ALBERT Question Answering System

This repository contains a simple yet powerful implementation of a question answering system based on ALBERT (A Lite BERT), a state-of-the-art natural language processing model developed by Google. The system leverages the ALBERT model to understand and extract answers from a given context for user-posed questions.

## Overview

The question answering system is designed to demonstrate the capabilities of the ALBERT model in processing and understanding natural language text. Users can input a paragraph of text as context and then ask questions based on that context. The system will analyze the input and provide the most relevant answer extracted directly from the provided context.

ALBERT stands for "A Lite BERT" and offers a more efficient alternative to the original BERT model, with reduced memory consumption and faster training times without significantly compromising performance.

## Features

- **Context-Based Question Answering:** Provide any paragraph of text as context, and the system will find answers to your questions based on this context.
- **Streamlit Interface:** A user-friendly web interface built with Streamlit, allowing for easy interaction with the model.


## Usage

To run the question answering system:

```
streamlit run p9.py
```

After starting the app, navigate to the displayed URL in your web browser. You will be prompted to enter a context (a paragraph of text) and a question based on that context. After submitting your question, the system will process the input and display the answer extracted from the context.

## Example

- **Context:** "ALBERT is a cutting-edge natural language processing model developed by Google. It stands for 'A Lite BERT' and is designed to provide a more efficient alternative to previous BERT models."

- **Question:** "What does ALBERT stand for?"

- **Answer:** "A Lite BERT"

![image](0329f44c0d66332b9507aa931b8105a.png)
![video](p9 · Streamlit - Google Chrome 2024-04-04 19-36-35.mp4)