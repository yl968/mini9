import streamlit as st
from transformers import AlbertTokenizer, AlbertForQuestionAnswering
import torch

st.title('ALBERT (Mini project 9)')
tokenizer = AlbertTokenizer.from_pretrained('albert-base-v2')
model = AlbertForQuestionAnswering.from_pretrained('albert-base-v2')

context = st.text_area("Context", "Mini Project 9")
question = st.text_input("Question", "Who developed ALBERT?")

if st.button('Answer'):

    inputs = tokenizer.encode_plus(question, context, add_special_tokens=True, return_tensors="pt")
    outputs = model(**inputs)
    answer_start_scores = outputs.start_logits
    answer_end_scores = outputs.end_logits
    answer_start = torch.argmax(answer_start_scores)
    answer_end = torch.argmax(answer_end_scores) + 1
    answer = tokenizer.decode(inputs["input_ids"][0][answer_start:answer_end], skip_special_tokens=True)

    if answer:
        st.write(f"Answer: {answer}")
    else:
        st.write("Unable to find an answer in the provided context.")